from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_friend
from .models import Friend

# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_friend(self):
        # Creating a new activity
        new_friend = Friend.objects.create(friend_name='Chino', npm='1906912041')

        # Retrieving all available activity
        count_friends = Friend.objects.all().count()
        self.assertEqual(count_friends, 1)
