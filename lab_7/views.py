from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    paginator = Paginator(mahasiswa_list,20)
    friend_list = Friend.objects.all()

    page = request.GET.get('page')
    try:
        mahasiswa_list = paginator.page(page)
    except PageNotAnInteger:
        mahasiswa_list = paginator.page(1)
    except EmptyPage:
        mahasiswa_list = paginator.page(paginator.num_pages)

    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    response['author'] = "Fardhan Dhiadribratha Sudjono"
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    response['author'] = "Fardhan Dhiadribratha Sudjono"
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        is_taken = False
        friend_list = Friend.objects.all()
        for i in friend_list :
            if i.npm == npm :
                is_taken = True
        if not is_taken:
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data = model_to_dict(friend)
            return HttpResponse(data)
        else:
            return HttpResponse(data)

@csrf_exempt
def delete_friend(request):
    if request.method == 'POST':
        friend_id = request.POST['id']
        print(friend_id)
        Friend.objects.filter(id=friend_id).delete()
        data = { 'status' : 'ok'}
        return JsonResponse(data)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    friend_list = Friend.objects.all()
    data = { 'is_taken' : False }
    for i in friend_list :
        if i.npm == npm :
            data = {
                'is_taken': True #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
            }
    return JsonResponse(data)

@csrf_exempt
def all_friends(request):
    friend_list = list(Friend.objects.all().values())
    data = {"json":friend_list}
    return JsonResponse(data)
    
def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data