from django.conf.urls import url
from .views import index, friend_list, add_friend, validate_npm, delete_friend, all_friends

urlpatterns = [
        url(r'^$', index, name='index'),
        url(r'^add-friend/$', add_friend, name='add-friend'),
        url(r'^validate-npm/$', validate_npm, name='validate-npm'),
        url(r'^delete-friend/$', delete_friend, name='delete-friend'),
        url(r'^get-friend-list/$', friend_list, name='friend_list'),
        url(r'^all-friends/$', all_friends, name='all-friends' ),
]
